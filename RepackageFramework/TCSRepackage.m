//
//  TCSRepackage.m
//  RepackageFramework
//
//  Created by Timothy Perfitt on 4/20/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSRepackage.h"
#import "TCSXMLParser.h"
#import "NSError+EasyError.h"
#import <AppKit/AppKit.h>
#import <xar/xar.h>
#import "Constants.h"
@implementation TCSRepackage

-(void)packageResourcesAtPath:(NSString *)inPath installLocation:(NSString *)inInstallLocation version:(NSString *)inVersion identifer:(NSString *)inIdentifer outputPackagePath:(NSString *)inOutputPackagePath completionBlock:(void (^)(NSError *err))completionBlock{


    BOOL isDir;
    NSString *tempPackageFile= [[NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]] stringByAppendingPathExtension:@"pkg"];

    NSFileManager *fm=[NSFileManager defaultManager];
    if ([[inPath pathExtension] isEqualToString:@""] && [fm fileExistsAtPath:inPath isDirectory:&isDir] && isDir==YES){
        //folder of stuff

        NSTask *productBuildTask=[[NSTask alloc] init];
        productBuildTask.launchPath=@"/usr/bin/pkgbuild";
        productBuildTask.arguments=@[@"--install-location",inInstallLocation,@"--identifier", inIdentifer, @"--root",inPath,@"--version",inVersion,tempPackageFile];
        [productBuildTask launch];
        [productBuildTask waitUntilExit];

        if (productBuildTask.terminationStatus!=0) {

            if ([fm fileExistsAtPath:tempPackageFile]){

                [fm removeItemAtPath:tempPackageFile error:nil];
            }
            completionBlock([NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred packaging up resources at path %@",inPath],NSLocalizedRecoverySuggestionErrorKey:@"Please check the resources and free space and try again."}]);


        }
        else{
            NSError *err;
            if([self convertPackageToMetaDistribution:tempPackageFile destination:inOutputPackagePath error:&err]==NO){
                if ([fm fileExistsAtPath:tempPackageFile]){

                    [fm removeItemAtPath:tempPackageFile error:nil];
                }
                completionBlock(err);
                return;
            }
            else {
                if ([fm fileExistsAtPath:tempPackageFile]){

                    [fm removeItemAtPath:tempPackageFile error:nil];
                }
                completionBlock(nil);

            }
        }
    }

}
-(void)repackagePackageAtPath:(NSString *)inPath destination:(NSString *)inDestination symbolicLinkPath:(NSString *)symbolicLinkPath forceEmbed:(BOOL)forceEmbed forceCopy:(BOOL)forceCopy cacheCopy:(BOOL)cacheCopy  status:(void (^)(NSString *))statusBlock completionBlock:(void (^)(NSError *err))completionBlock{
    

    [self repackagePackageAtPath:inPath destination:inDestination forceEmbed:forceEmbed forceCopy:forceCopy status:statusBlock completionBlock:^(NSError * _Nonnull err) {

        if (!err) {
            NSFileManager *fm=[NSFileManager defaultManager];

            if([fm createSymbolicLinkAtPath:symbolicLinkPath withDestinationPath:inDestination error:&err]==NO) {
                completionBlock(err);
                return;
            }

        }
        completionBlock(err);
    }];

}

-(void)repackagePackageAtPath:(NSString *)inPath destination:(NSString *)inDestination forceEmbed:(BOOL)forceEmbed forceCopy:(BOOL)forceCopy status:(void (^)(NSString *))statusBlock completionBlock:(void (^)(NSError *err))completionBlock
{


    NSError *err;
    BOOL isDir;
    NSFileManager *fm=[NSFileManager defaultManager];
    NSString *pathExt=[[inPath pathExtension] lowercaseString];

    NSString *appPackageDestinationPath=inDestination;

    if ([pathExt isEqualToString:@"app"]){
        statusBlock([NSString stringWithFormat:@"Converting %@ to package",inPath.lastPathComponent]);
        if([self convertApplication:inPath toPackage:appPackageDestinationPath error:&err]==NO){
            completionBlock(err);
            return;
        }
    }


    else if (([pathExt isEqualToString:@"sparsebundle"] || [pathExt isEqualToString:@"dmg"] )) {

        statusBlock([NSString stringWithFormat:@"Embedding bundle package and copying %@",inPath.lastPathComponent]);

        if ([fm fileExistsAtPath:appPackageDestinationPath]==NO) {

            NSLog(@"Package is dmg or/sparsebundle. Embedding in a flat package with a postinstall to install the package inside. DMG is converted to sparsebundle because the installer hates large single files due to xar compression.");

            NSString *tempPackageRoot= [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]] ;

            if( [fm createDirectoryAtPath:tempPackageRoot withIntermediateDirectories:YES attributes:nil error:nil]==NO) {
                completionBlock([NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred creating the temporary folder for the disk image. %@",tempPackageRoot],NSLocalizedRecoverySuggestionErrorKey:@"Please check the volume and try again."}]);
                return;

            }

            [self embedDiskImage:inPath intoPackage:appPackageDestinationPath error:&err status:statusBlock];
        }
    }
    else if (forceCopy==NO && ( forceEmbed==YES || ([[[inPath pathExtension] lowercaseString] isEqualToString:@"mpkg"] || ([pathExt isEqualToString:@"pkg"]   && [fm fileExistsAtPath:inPath isDirectory:&isDir] && isDir==YES)))) {


        if ([fm fileExistsAtPath:appPackageDestinationPath]==NO) {

            NSLog(@"Wrapping and embedding in a flat package");


            NSString *tempPackageRoot= [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]] ;

            if( [fm createDirectoryAtPath:tempPackageRoot withIntermediateDirectories:NO attributes:nil error:nil]==NO) {
                completionBlock([NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred creating the temporary folder for the MPKG. %@",tempPackageRoot],NSLocalizedRecoverySuggestionErrorKey:@"Please check the volume and try again."}]);

                return;

            }

            if([fm copyItemAtPath:inPath toPath:[tempPackageRoot stringByAppendingPathComponent:[inPath lastPathComponent]] error:nil]==NO) {
                completionBlock([NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred copying the MPKG. %@",inPath],NSLocalizedRecoverySuggestionErrorKey:@"Please check the volume and try again."}]);

                return;

            }


            statusBlock([NSString stringWithFormat:@"Embedding bundle package and copying %@",inPath.lastPathComponent]);
            NSString *tempScriptsFolderPath= [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]] ;

            if([fm createDirectoryAtPath:tempScriptsFolderPath withIntermediateDirectories:NO attributes:nil error:nil]==NO){
                completionBlock([NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred createing the temporary script folder. %@",tempScriptsFolderPath],NSLocalizedRecoverySuggestionErrorKey:@"Please check the volume and try again."}]);

                return;

            }

            NSString *script=[NSString stringWithFormat:@"#!/bin/sh -x\n/usr/sbin/installer -package \"$2/%@\" -target \"$3\"",[inPath lastPathComponent]];

            NSData *scriptData=[script dataUsingEncoding:NSUTF8StringEncoding];


            if( [scriptData writeToFile:[tempScriptsFolderPath stringByAppendingPathComponent:@"postinstall"] atomically:NO]==NO) {

                completionBlock([NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred writing the post install script  %@",scriptData],NSLocalizedRecoverySuggestionErrorKey:@"Please check the volume and try again."}]);

                return;

            }
            if([fm setAttributes:@{NSFilePosixPermissions:[NSNumber numberWithShort:0755]} ofItemAtPath:[tempScriptsFolderPath stringByAppendingPathComponent:@"postinstall"] error:&err]==NO) {
                completionBlock(err);

                return;
            }
            NSString *packageUUID=[NSUUID UUID].UUIDString;

            NSString *tempPackageFile= [[NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]] stringByAppendingPathExtension:@"pkg"];

            NSTask *productBuildTask=[[NSTask alloc] init];
            productBuildTask.launchPath=@"/usr/bin/pkgbuild";
            productBuildTask.arguments=@[@"--install-location",@"/private/tmp",@"--identifier", packageUUID, @"--root",tempPackageRoot,@"--scripts",tempScriptsFolderPath,tempPackageFile];
            [productBuildTask launch];
            [productBuildTask waitUntilExit];


            if (productBuildTask.terminationStatus!=0) {

                completionBlock([NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred embedding the package %@",tempPackageFile],NSLocalizedRecoverySuggestionErrorKey:@"Please check the package and free space and try again."}]);


            }
            else{
                statusBlock([NSString stringWithFormat:@"Converting %@",appPackageDestinationPath.lastPathComponent]);

                if([self convertPackageToMetaDistribution:tempPackageFile destination:appPackageDestinationPath error:&err]==NO){
                    completionBlock(err);
                    return;
                }
            }


            NSError *err;

            if([fm removeItemAtPath:tempPackageFile error:&err]==NO){
                completionBlock(err);
                return;
            }
        }
        else {


            completionBlock([NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Package exists at  %@",appPackageDestinationPath],NSLocalizedRecoverySuggestionErrorKey:@"Please check the package and free space and try again."}]);
        }
    }
    else {

        NSError *err;
        BOOL isDistribution=[self isDistribution:inPath error:&err];
        if (err){
            completionBlock(err);
        }

        [fm fileExistsAtPath:inPath isDirectory:&isDir];
        if (forceCopy==YES ||isDistribution==YES){

            NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
            BOOL shouldSign=[ud boolForKey:@"shouldSignPackages"];
            NSString *signingIdentity=[ud objectForKey:@"signingIdentity"];

            if (isDir==NO && shouldSign==YES && signingIdentity && signingIdentity.length>0){
                statusBlock([NSString stringWithFormat:@"Signing %@",inPath.lastPathComponent]);
                NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
                BOOL skipIfAlreadySigned=[ud boolForKey:@"shouldSkipIfAlreadySigned"];
                if([self signPackage:inPath identity:signingIdentity skipIfAlreadySigned:skipIfAlreadySigned outputPath:appPackageDestinationPath error:&err]==NO) {

                    completionBlock(err);
                }

            }
            else {
                statusBlock([NSString stringWithFormat:@"Copying %@",inPath.lastPathComponent]);
                if([fm copyItemAtPath:inPath toPath:appPackageDestinationPath error:&err]==NO) {
                    completionBlock(err);
                }
            }

        }
        else {
            statusBlock([NSString stringWithFormat:@"Converting and copying %@",inPath.lastPathComponent]);
            if([self convertPackageToMetaDistribution:inPath destination:appPackageDestinationPath error:&err]==NO){

                completionBlock(err);
                return;
            }
        }
    }

    completionBlock(nil);
}
-(BOOL)isDistribution:(NSString *)inPath error:(NSError * __autoreleasing *)error{
    NSFileManager *fm=[NSFileManager defaultManager];
    BOOL isDir;
    if ([[inPath.pathExtension lowercaseString] isEqualToString:@"pkg"]==NO) return NO;
    

    if ([fm fileExistsAtPath:inPath isDirectory:&isDir] && isDir==YES) return NO;

    if ([[NSWorkspace sharedWorkspace] isFilePackageAtPath:inPath]==YES) return NO;
    NSError *attributesError;
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:inPath error:&attributesError];

    NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
    long long fileSize = [fileSizeNumber longLongValue];
    if (fileSize ==0) {


        *error=[NSError easyErrorWithTitle:@"Package size Error"
                                      body:[NSString stringWithFormat:@"An error occurred when repackaging %@. The item size is 0. Attributes Error: %@",inPath,attributesError.localizedDescription]
                                      line:__LINE__
                                      file:@__FILE__];



        return NO;
    }
    xar_t x;
    xar_file_t f;
    xar_iter_t i;

    x = xar_open(inPath.UTF8String, READ);

    if( x == NULL ) {
        *error=[NSError easyErrorWithTitle:@"Error opening archive"
                                      body:[NSString stringWithFormat:@"An error occurred when opening %@",inPath]
                                      line:__LINE__
                                      file:@__FILE__];
        return NO;
    }

    i = xar_iter_new();
    if( !i ) {
        fprintf(stderr, "Error obtaining a new iterator\n");
        exit(1);
    }
    BOOL isDistribution=NO;
    for( f = xar_file_first(x, i); f; f = xar_file_next(i) ) {
        const char *type,*name;

        xar_prop_get(f, "name", &name);
        xar_prop_get(f, "type", &type);

        if (type && name ) {
            NSString *internalType=[NSString stringWithUTF8String:type];
            NSString *internalName=[NSString stringWithUTF8String:name];

            if ([internalType.lowercaseString isEqualToString:@"file"] &&
                [internalName.lowercaseString isEqualToString:@"distribution"]){
                isDistribution=YES;

            }
        }

    }

    xar_iter_free(i);
    xar_close(x);

    return isDistribution;
}
-(BOOL)convertPackageToMetaDistribution:(NSString *)inPath destination:(NSString *)inDestination error:(NSError **)error{

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
    BOOL shouldSign=[ud boolForKey:@"shouldSignPackages"];
    NSString *signingIdentity=[ud objectForKey:@"signingIdentity"];

    NSTask *productBuildTask=[[NSTask alloc] init];
    productBuildTask.launchPath=@"/usr/bin/productbuild";

    NSString *filename=[[NSUUID UUID] UUIDString];
    NSString *plistPath=[NSTemporaryDirectory() stringByAppendingFormat:@"%@.plist",filename];

    NSError *err;
    if([@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\"><plist version=\"1.0\"><dict>        <key>arch</key>        <array>                <string>x86_64</string>                <string>arm64</string>        </array></dict></plist>" writeToFile:plistPath atomically:NO encoding:NSUTF8StringEncoding error:&err]==NO){

        NSLog(@"error write product plist");
        return NO;
    }



    productBuildTask.arguments=@[@"--product",plistPath,@"--package",inPath,inDestination];

    if (shouldSign==YES && signingIdentity && signingIdentity.length>0){
        productBuildTask.arguments=@[@"--package",inPath,@"--sign",signingIdentity,inDestination];
    }
    [productBuildTask launch];
    [productBuildTask waitUntilExit];

    if (productBuildTask.terminationStatus!=0){
        NSString *command=[NSString stringWithFormat:@"%@ %@",productBuildTask.launchPath,productBuildTask.arguments];
        *error=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Could not convert package %@. Verify package and certificate used for signing (if applicable). Command was: %@. Try running  in Terminal to verify error.",inPath.lastPathComponent,command]}];
        return NO;
    }
    return YES;
}

-(BOOL)convertApplication:(NSString *)inApplicationPath toPackage:(NSString *)inDestination error:(NSError **)error{
    NSFileManager *fm=[NSFileManager defaultManager];

    NSString *tempPackageFile= [[NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]] stringByAppendingPathExtension:@"pkg"];

    NSString *script=[NSString stringWithFormat:@"#!/bin/sh\n#Copyright 2018-2019 Twocanoes Software, Inc\n\n/usr/sbin/chown -R root:wheel \"/Applications/%@\"\n/bin/chmod -R 755 \"/Applications/%@\"\n",inApplicationPath.lastPathComponent,inApplicationPath.lastPathComponent];

    NSString *tempScriptsFolderPath= [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]] ;

    if([fm createDirectoryAtPath:tempScriptsFolderPath withIntermediateDirectories:YES attributes:nil error:nil]==NO){
        *error=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred createing the temporary script folder. %@",tempScriptsFolderPath],NSLocalizedRecoverySuggestionErrorKey:@"Please check the volume and try again."}];

        return NO;

    }


    NSData *scriptData=[script dataUsingEncoding:NSUTF8StringEncoding];

    if( [scriptData writeToFile:[tempScriptsFolderPath stringByAppendingPathComponent:@"postinstall"] atomically:NO]==NO) {

        *error=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred writing the post install script  %@",scriptData],NSLocalizedRecoverySuggestionErrorKey:@"Please check the volume and try again."}];

        return NO;

    }
    if([fm setAttributes:@{NSFilePosixPermissions:[NSNumber numberWithShort:0755]} ofItemAtPath:[tempScriptsFolderPath stringByAppendingPathComponent:@"postinstall"] error:error]==NO) {

        return NO;
    }


    NSTask *productBuildTask=[[NSTask alloc] init];
    productBuildTask.launchPath=@"/usr/bin/pkgbuild";
    productBuildTask.arguments=@[@"--install-location",@"/Applications",@"--component",inApplicationPath,tempPackageFile];
    NSLog(@"%@",[productBuildTask.arguments componentsJoinedByString:@" "]);
    [productBuildTask launch];
    [productBuildTask waitUntilExit];

    if (productBuildTask.terminationStatus!=0) {
        *error=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred converting the package %@",inApplicationPath],NSLocalizedRecoverySuggestionErrorKey:@"Please check the package and free space and try again."}];
        return NO;

    }
    else {
        if([self convertPackageToMetaDistribution:tempPackageFile destination:inDestination error:error]==NO){

            return NO;
        }
    }
    if([fm fileExistsAtPath:tempPackageFile] && [fm removeItemAtPath:tempPackageFile error:error]==NO){
        return NO;
    }

    return YES;
}
-(BOOL)signPackage:(NSString *)inPackage identity:(NSString *)inIdentity skipIfAlreadySigned:(BOOL)skipIfAlreadySigned outputPath:(NSString *)outputPath error:(NSError **)err{


    //pkgutil --check-signature

    BOOL isSigned=NO;
    if (skipIfAlreadySigned==YES) {
        NSTask *pkgutilTask=[[NSTask alloc] init];
        pkgutilTask.launchPath=@"/usr/sbin/pkgutil";


        pkgutilTask.arguments=@[@"--check-signature",inPackage];
        [pkgutilTask launch];
        [pkgutilTask waitUntilExit];

        if (pkgutilTask.terminationStatus==0){
            isSigned=YES;
        }

    }


    if ((skipIfAlreadySigned==YES && isSigned==NO) || skipIfAlreadySigned==NO){
        NSTask *productBuildTask=[[NSTask alloc] init];
        productBuildTask.launchPath=@"/usr/bin/productsign";


        productBuildTask.arguments=@[@"--sign",inIdentity,inPackage,outputPath];
        [productBuildTask launch];
        [productBuildTask waitUntilExit];

        if (productBuildTask.terminationStatus!=0){
            *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Signing failed for %@",inPackage.lastPathComponent]}];
            return NO;
        }

    }
    else {
        NSFileManager *fm=[NSFileManager defaultManager];

        if([fm copyItemAtPath:inPackage toPath:outputPath error:err]==NO) {
            return NO;
        }

    }

    return YES;


}

-(BOOL)embedDiskImage:(NSString *)inFilePath intoPackage:(NSString *)inPackageDestinationPath error:(NSError **)err status:(void (^)(NSString *))statusBlock{

    NSFileManager *fm=[NSFileManager defaultManager];

    NSString *pathExt=[[inFilePath pathExtension] lowercaseString];
    NSString *tempPackageRoot= [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]] ;

    if([fm createDirectoryAtPath:tempPackageRoot withIntermediateDirectories:YES attributes:nil error:err]==NO) {
        return NO;
    }
    NSString *uuidString=[[NSUUID UUID] UUIDString];
    NSString *destinationDMGName=[uuidString stringByAppendingPathExtension:@"sparsebundle"];

    if ([pathExt isEqualToString:@"dmg"])  {
        //We have a dmg so creating a sparsebundle
        NSString *dmgSparseBundleFilePath=[tempPackageRoot stringByAppendingPathComponent:destinationDMGName];
        NSTask *dmgBuildTask=[[NSTask alloc] init];
        dmgBuildTask.launchPath=@"/usr/bin/hdiutil";
        dmgBuildTask.arguments=@[@"create",@"-size",@"999G",@"-fs",@"HFS+",@"-type",@"SPARSEBUNDLE",@"-volname",uuidString,dmgSparseBundleFilePath];
        [dmgBuildTask launch];
        [dmgBuildTask waitUntilExit];

        if (dmgBuildTask.terminationStatus!=0) {

            *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred while creating  the dmg %@",dmgSparseBundleFilePath],NSLocalizedRecoverySuggestionErrorKey:@"Please check the package and free space and try again."}];
            return NO;

        }
        statusBlock([NSString stringWithFormat:@"Creating Disk Image for %@",inFilePath.lastPathComponent]);
        NSTask *dmgMountTask=[[NSTask alloc] init];
        dmgMountTask.launchPath=@"/usr/bin/hdiutil";
        dmgMountTask.arguments=@[@"mount",dmgSparseBundleFilePath];
        [dmgMountTask launch];
        [dmgMountTask waitUntilExit];

        if (dmgMountTask.terminationStatus!=0) {

            *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred while mounting  the dmg %@",inFilePath],NSLocalizedRecoverySuggestionErrorKey:@"Please check the package and free space and try again."}];
            return NO;

        }


        NSString *mountedDMGPath=[NSString stringWithFormat:@"/Volumes/%@",uuidString];
        if (![fm fileExistsAtPath:mountedDMGPath]){
            *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"The sparsebundle dmg volume mount not found at  %@",mountedDMGPath],NSLocalizedRecoverySuggestionErrorKey:@"Please check the package and free space and try again."}];
        }

        statusBlock([NSString stringWithFormat:@"Copy files to disk image for %@",inFilePath.lastPathComponent]);

        //copying dmg contents to sparsebundle
        if([fm copyItemAtPath:inFilePath toPath:[mountedDMGPath stringByAppendingPathComponent:inFilePath.lastPathComponent] error:nil]==NO) {
            *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred copying the disk image. %@",inFilePath],NSLocalizedRecoverySuggestionErrorKey:@"Please check the volume and try again."}];
        }

        statusBlock([NSString stringWithFormat:@"Unmounting Disk Image for  %@",inFilePath.lastPathComponent]);
        //unmounting dmg
        NSTask *dmgUnMountTask=[[NSTask alloc] init];
        dmgUnMountTask.launchPath=@"/usr/sbin/diskutil";
        dmgUnMountTask.arguments=@[@"unmount",mountedDMGPath];
        [dmgUnMountTask launch];
        [dmgUnMountTask waitUntilExit];

        if (dmgUnMountTask.terminationStatus!=0) {

            *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred while unmounting  the dmg %@",inFilePath],NSLocalizedRecoverySuggestionErrorKey:@"Please check the package and free space and try again."}];

        }
    }
//    else {
//        //we have a sparsebundle, not dmg so just copying that to temp folder.
//
//        statusBlock([NSString stringWithFormat:@"Copying disk image for %@",inFilePath.lastPathComponent]);
//        if([fm copyItemAtPath:inFilePath toPath:destinationDMGName error:err]==NO) {
//            return NO;
//        }
//    }


    NSString *tempScriptsFolderPath= [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]] ;

    if([fm createDirectoryAtPath:tempScriptsFolderPath withIntermediateDirectories:NO attributes:nil error:nil]==NO){
        *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred creating the temporary script folder. %@",tempScriptsFolderPath],NSLocalizedRecoverySuggestionErrorKey:@"Please check the volume and try again."}];

        return NO;

    }
//    NSString *script=@"#!/bin/sh -x\n#Copyright 2018-2019 Twocanoes Software, Inc\n\ndiskimage_name=\"{{diskimage_name}}\"\nvol=$(hdiutil attach -plist \"$2/${diskimage_name}\" |grep -A 1 mount-point|grep -v mount-point|sed 's|.*<string>\\(.*\\)</string>.*|\\1|')\n\nfor pkg in \"${vol}\"/*.pkg; do\n    /usr/sbin/installer -package \"${pkg}\" -target \"$3\"\ndone\nfor app in \"${vol}\"/*.app; do\napp_name=$(basename \"${app}\")\n/bin/cp -R \"${app}\" \"$3\"/Applications/\nchown -R root:wheel \"$3\"/Applications/\"${app_name}\"\n\n    done\n";


    NSString *scriptPath=[[NSBundle bundleWithIdentifier:@"com.twocanoes.RepackageFramework"] pathForResource:@"com.twocanoes.mds.installpkg" ofType:@"sh"];

    NSString *script=[NSString stringWithContentsOfFile:scriptPath encoding:NSUTF8StringEncoding error:nil];

    script=[script stringByReplacingOccurrencesOfString:@"{{diskimage_name}}" withString:destinationDMGName];
    NSData *scriptData=[script dataUsingEncoding:NSUTF8StringEncoding];

    if( [scriptData writeToFile:[tempScriptsFolderPath stringByAppendingPathComponent:@"postinstall"] atomically:NO]==NO) {

        *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred writing the post install script  %@",scriptData],NSLocalizedRecoverySuggestionErrorKey:@"Please check the volume and try again."}];

        return NO;

    }
    if([fm setAttributes:@{NSFilePosixPermissions:[NSNumber numberWithShort:0755]} ofItemAtPath:[tempScriptsFolderPath stringByAppendingPathComponent:@"postinstall"] error:err]==NO) {

        return NO;
    }


    NSString *packageUUID=[NSUUID UUID].UUIDString;

    NSString *tempPackageFile= [[NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]] stringByAppendingPathExtension:@"pkg"];


    //building package from folder that has disk image in it.
    NSTask *productBuildTask=[[NSTask alloc] init];
    productBuildTask.launchPath=@"/usr/bin/pkgbuild";
    productBuildTask.arguments=@[@"--install-location",@"/private/tmp",@"--identifier", packageUUID, @"--root",tempPackageRoot,@"--scripts",tempScriptsFolderPath,tempPackageFile];
    [productBuildTask launch];
    [productBuildTask waitUntilExit];


    if (productBuildTask.terminationStatus!=0) {

        *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred embedding the package %@",tempPackageFile],NSLocalizedRecoverySuggestionErrorKey:@"Please check the package and free space and try again."}];
        return NO;


    }
    //creating a flat meta package.
    statusBlock([NSString stringWithFormat:@"Building  final package for %@",inFilePath.lastPathComponent]);

    if([self convertPackageToMetaDistribution:tempPackageFile destination:inPackageDestinationPath error:err]==NO){

        return NO;

    }
    //remove temporary files
    if([fm removeItemAtPath:tempPackageRoot error:err]==NO){
        *err=[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"An error occurred removing temp files:%@",tempPackageRoot],NSLocalizedRecoverySuggestionErrorKey:@"Please check permissions."}];

    }
    return YES;
}
@end
