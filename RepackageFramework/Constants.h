//
//  Constants.h
//  Repackage
//
//  Created by Timothy Perfitt on 11/29/20.
//  Copyright © 2020 Twocanoes Software. All rights reserved.
//

#ifndef Constants_h
#define Constants_h


#define PACKAGEREQUIREMENTSPLIST @"<?xml version="1.0" encoding="UTF-8"?> \
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" \ "http://www.apple.com/DTDs/PropertyList-1.0.dtd"> \
<plist version="1.0"> \
<dict>\
<array> \
        <string>x86_64</string> \
        <string>arm64</string> \
</array> \
</dict> \
</plist> \
"
#endif /* Constants_h */
