//
//  TCSRepackage.h
//  RepackageFramework
//
//  Created by Timothy Perfitt on 4/20/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSRepackage : NSObject
-(void)packageResourcesAtPath:(NSString *)inPath installLocation:(NSString *)inInstallLocation version:(NSString *)inVersion identifer:(NSString *)inIdentifer outputPackagePath:(NSString *)inOutputPackagePath completionBlock:(void (^)(NSError *err))completionBlock;

-(BOOL)convertPackageToMetaDistribution:(NSString *)inPath destination:(NSString *)inDestination error:(NSError **)error;
-(void)repackagePackageAtPath:(NSString *)inPath destination:(NSString *)inDestination forceEmbed:(BOOL)forceEmbed forceCopy:(BOOL)forceCopy status:(void (^)(NSString *))statusBlock completionBlock:(void (^)(NSError *err))completionBlock;

-(void)repackagePackageAtPath:(NSString *)inPath destination:(NSString *)inDestination symbolicLinkPath:(NSString *)symbolicLinkPath forceEmbed:(BOOL)forceEmbed forceCopy:(BOOL)forceCopy cacheCopy:(BOOL)cacheCopy  status:(void (^)(NSString *))statusBlock completionBlock:(void (^)(NSError *err))completionBlock;


-(BOOL)signPackage:(NSString *)inPackage identity:(NSString *)inIdentity skipIfAlreadySigned:(BOOL)skipIfAlreadySigned outputPath:(NSString *)outputPath error:(NSError **)err;
-(BOOL)isDistribution:(NSString *)inPath error:(NSError **)error;
@end

NS_ASSUME_NONNULL_END
