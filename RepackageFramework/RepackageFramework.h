//
//  RepackageFramework.h
//  RepackageFramework
//
//  Created by Timothy Perfitt on 4/20/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for RepackageFramework.
FOUNDATION_EXPORT double RepackageFrameworkVersionNumber;

//! Project version string for RepackageFramework.
FOUNDATION_EXPORT const unsigned char RepackageFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RepackageFramework/PublicHeader.h>

#import <RepackageFramework/TCSRepackage.h>
