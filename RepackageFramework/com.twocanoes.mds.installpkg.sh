#!/bin/sh

#Copyright 2018-2019 Twocanoes Software, Inc
shopt -s nullglob

install_dir="$2"
target="$3"
diskimage_name="{{diskimage_name}}"
echo "mounting sparse image"
mounted_sparse=$(hdiutil attach -plist "${install_dir}/${diskimage_name}" |grep -A 1 mount-point|grep -v mount-point|sed 's|.*<string>\(.*\)</string>.*|\1|')
echo "mounting dmg"
vol=$(hdiutil attach -plist "${mounted_sparse}"/*.dmg |grep -A 1 mount-point|grep -v mount-point|sed 's|.*<string>\(.*\)</string>.*|\1|')

for pkg in "${vol}"/*.pkg; do
    echo "installing ${pkg}"
    /usr/sbin/installer -package "${pkg}" -target "${target}"

done

for app in "${vol}"/*.app; do

    app_name=$(basename "${app}")
    echo "copying ${app_name}"
    /bin/cp -R "${app}" "$3"/Applications/
    echo "changing ownership"
    chown -R root:wheel "$3"/Applications/"${app_name}"

done
diskutil unmount force "${vol}"
diskutil unmount force "${mounted_sparse}"

exit 0
