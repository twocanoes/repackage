#!/bin/sh

echo "Uninstalling"
dir=`/usr/bin/mktemp -d ~/.Trash/Repackage.XXXXXX`
    /bin/chmod ugo+rx "${dir}"

if [ -d "/Applications/Repackage.app" ] ; then 
    mv "/Applications/Repackage.app" "${dir}"; 
fi
	