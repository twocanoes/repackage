#!/bin/sh -x
set -e 
PRODUCT_NAME="Repackage"
PRODUCT_SETTINGS_PATH=./Info.plist


###########################
CURRENT_PATH=$(dirname $0)

#buildNumber=$(/usr/libexec/PlistBuddy -c "Print CFBundleVersion" "${PRODUCT_SETTINGS_PATH}")
buildNumber=$(agvtool what-version -terse)
temp_folder=$(mktemp -d "/tmp/${PRODUCT_NAME}.XXXXXXXX")

xcodebuild archive -project "${PRODUCT_NAME}.xcodeproj" -scheme "${PRODUCT_NAME}" -archivePath  "${temp_folder}/${PRODUCT_NAME}.xcarchive"

xcodebuild -exportArchive -archivePath "${temp_folder}/${PRODUCT_NAME}.xcarchive"  -exportOptionsPlist "${CURRENT_PATH}/build/exportOptions.plist" -exportPath "${temp_folder}/build"

echo saving symbols
mkdir -p "${CURRENT_PATH}/build/symbols/${buildNumber}"


cp -R "${temp_folder}/${PRODUCT_NAME}.xcarchive/dSYMs/${PRODUCT_NAME}.app.dSYM" "${CURRENT_PATH}/build/symbols/${buildNumber}/"


open "${temp_folder}/build" 
if [ $? -ne 0 ]; then 

exit 
fi

/Users/tperfitt/Documents/Projects/build/build.sh  "${CURRENT_PATH}/build" "${CURRENT_PATH}" "${PRODUCT_NAME}" "${temp_folder}/build/${PRODUCT_NAME}.app"

echo Please commit with git commit -a -m 'bumped version'
