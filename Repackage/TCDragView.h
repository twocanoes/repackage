//
//  TCDragView.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/26/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol TCItemDraggedProtocol <NSObject>

-(void)itemDropped:(NSArray * _Nonnull)inPath;

@end
NS_ASSUME_NONNULL_BEGIN

@interface TCDragView : NSView <NSDraggingDestination>
@property (weak) IBOutlet id <TCItemDraggedProtocol> delegate;

@end

NS_ASSUME_NONNULL_END
