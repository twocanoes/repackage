//
//  TCSXMLParser.m
//  Repackage
//
//  Created by Timothy Perfitt on 9/25/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSXMLParser.h"
@interface TCSXMLParser()

@property (strong) NSXMLParser *parser;
@property (strong) NSString *currentName;
@property (assign) BOOL returnIsDistribution;
@property (assign) int level;
@end
@implementation TCSXMLParser

-(BOOL)isDistribution:(NSData *)inData{
    self.level=0;
    self.returnIsDistribution=NO;
    self.parser=[[NSXMLParser alloc] initWithData:inData];
    self.parser.delegate=self;
    [self.parser parse];

    return self.returnIsDistribution;

}


- (void)parserDidStartDocument:(NSXMLParser *)parser {
  //the parser started this document. what are you going to do?
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {


    self.currentName=[elementName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

       self.level++;
  //the parser found an XML tag and is giving you some information about it
  //what are you going to do?
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName{
    self.level--;


}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
  //the parser found some characters inbetween an opening and closing tag
  //what are you going to do?
    if ([self.currentName isEqualToString:@"name"] && self.level==4 && [string isEqualToString:@"Distribution"]) {
        self.returnIsDistribution=YES;
        [self.parser abortParsing];
        
    }


}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    self.returnIsDistribution=NO;

}
@end
