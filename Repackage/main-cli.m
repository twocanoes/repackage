//
//  main.m
//  repackage
//
//  Created by Timothy Perfitt on 4/20/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <getopt.h>



#import "/Users/tperfitt/Documents/Projects/Repackage/RepackageFramework/TCSRepackage.h"
int main(int argc,  char * argv[]) {
    @autoreleasepool {

        int bflag, ch;
        BOOL force=NO;
        /* options descriptor */
         struct option longopts[] = {
            { "force-wrap",      no_argument,            NULL,           'f' }

         };

        bflag = 0;
        while ((ch = getopt_long(argc, argv, "f", longopts, NULL)) != -1) {
            switch (ch) {
                case 'f':
                    force=YES;
                    break;
//                default:
//                    usage();
            }
        }
        argc -= optind;
        argv += optind;

        if (argc!=1) {
            fprintf(stderr,"specify a package, image, or application to package\n");
            exit (-1);
        }
        TCSRepackage *repackage=[[TCSRepackage alloc] init];

        NSString *currFile=[NSString stringWithUTF8String:argv[0]];
        NSError *err;
        NSFileManager *fm=[NSFileManager defaultManager];
        NSString *newFolder=[[currFile stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"converted"];

        if (![fm fileExistsAtPath:newFolder]){

            if([fm createDirectoryAtPath:newFolder withIntermediateDirectories:NO attributes:nil error:&err]==NO) {
                fprintf(stderr,"%s\n",[err.localizedDescription UTF8String]);

            }
        }

        NSString *destinationPath=[newFolder stringByAppendingPathComponent:[[[currFile   lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:@"pkg"]];


        [repackage repackagePackageAtPath:currFile destination:destinationPath forceEmbed:force forceCopy:NO status:^(NSString * status) {

            fprintf(stderr,"%s\n",status.UTF8String);
        } completionBlock:^(NSError * _Nonnull err) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!err) {
                    NSLog(@"Done!");
                }
                else {
                    fprintf(stderr,"%s\n",[err.localizedDescription UTF8String]);
                }
            });
        }];

    }
    return 0;
}
