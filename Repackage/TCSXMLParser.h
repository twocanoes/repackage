//
//  TCSXMLParser.h
//  Repackage
//
//  Created by Timothy Perfitt on 9/25/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSXMLParser : NSObject <NSXMLParserDelegate>
-(BOOL)isDistribution:(NSData *)inData;
@end

NS_ASSUME_NONNULL_END
