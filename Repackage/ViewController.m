//
//  ViewController.m
//  Repackage
//
//  Created by Timothy Perfitt on 4/20/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "ViewController.h"
#import "TCSRepackage.h"
@interface ViewController()

@property (assign) BOOL isRunning;
@property (assign) BOOL shouldCancel;
@property (strong) NSString *packagingStatus;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isRunning=NO;
    [[NSNotificationCenter defaultCenter] addObserverForName:@"TCSFilesDropped" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {

        NSArray *array=[[note userInfo] objectForKey:@"filenames"];
        [self convertFiles:array];
    }];

}
-(void)itemDropped:(NSArray *)droppedItems{


    [self convertFiles:droppedItems];
}
- (IBAction)cancelButtonPressed:(id)sender {
    self.shouldCancel=YES;
}

-(void)convertFiles:(NSArray *)filenames{
    self.shouldCancel=NO;
    TCSRepackage *repackage=[[TCSRepackage alloc] init];
    self.isRunning=YES;
    __block NSString *newFolder;

        NSOpenPanel *openPanel=[NSOpenPanel openPanel];
        openPanel.canChooseDirectories=YES;
        openPanel.canChooseFiles=NO;
        openPanel.message=@"Select a destination folder to save converted packages";


        NSInteger res=[openPanel runModal];

        if (res==NSModalResponseOK) {

            newFolder=openPanel.URL.path;

        }
        else {
            return;
        }

    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
        [filenames enumerateObjectsUsingBlock:^(NSString * currFile, NSUInteger idx, BOOL * _Nonnull stop) {

            if (self.shouldCancel==YES) return;

            NSError *err;
            NSFileManager *fm=[NSFileManager defaultManager];
//            NSString *newFolder=[[currFile stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"converted"];


            if (![fm fileExistsAtPath:newFolder]){

                if([fm createDirectoryAtPath:newFolder withIntermediateDirectories:NO attributes:nil error:&err]==NO) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSAlert alertWithError:err] runModal];
                        return;
                    });
                }
            }

            NSString *destinationPath=[newFolder stringByAppendingPathComponent:[[[currFile   lastPathComponent] stringByDeletingPathExtension] stringByAppendingPathExtension:@"pkg"]];

            BOOL forceEmbed=[[NSUserDefaults standardUserDefaults] boolForKey:@"shouldForceEmbedding"];

            dispatch_async(dispatch_get_main_queue(), ^{

                self.packagingStatus=[NSString stringWithFormat:@"Packaging %@",currFile.lastPathComponent];
            });

            [repackage repackagePackageAtPath:currFile destination:destinationPath forceEmbed:forceEmbed forceCopy:NO status:^(NSString * status) {

                dispatch_async(dispatch_get_main_queue(), ^{

                    self.packagingStatus=status;
                });
                NSLog(@"Status: %@",status);
            } completionBlock:^(NSError * _Nonnull err) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (err) {
                        [[NSAlert alertWithError:err] runModal];
                    }
                });
            }];


        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.isRunning=NO;
            self.packagingStatus=@"";
        });
    });

}
- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}



@end
