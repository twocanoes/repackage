//
//  TCSPreferenceViewController.h
//  Repackage
//
//  Created by Timothy Perfitt on 4/22/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSPreferenceViewController : NSViewController

@end

NS_ASSUME_NONNULL_END
