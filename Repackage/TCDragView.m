//
//  TCDragView.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/26/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCDragView.h"
@interface TCDragView()

@end
@implementation TCDragView
- (instancetype)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if (self) {
        [self  registerForDraggedTypes: [NSArray arrayWithObjects: NSFilenamesPboardType, nil]];

    }
    return self;
}


- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender{
//    NSPasteboard *pb;
//    NSString *p;

//    pb = [sender draggingPasteboard];
//    if ( [[pb types] containsObject:NSFilenamesPboardType] ) {
//        NSArray *fnames = [ pb propertyListForType:NSFilenamesPboardType];
//        p = [fnames objectAtIndex:0];
//        if (![[[p pathExtension] lowercaseString] isEqualToString:@"dmg"]) return NSDragOperationNone;
//    }



    return NSDragOperationCopy;
}
- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender{
    NSPasteboard *pb;
    pb = [sender draggingPasteboard];
    if ( [[pb types] containsObject:NSFilenamesPboardType] ) {
        NSArray *fnames = [ pb propertyListForType:NSFilenamesPboardType];
        if ([self.delegate respondsToSelector:@selector(itemDropped:)]){
            [self.delegate itemDropped:fnames];
        }

    }
    return YES;
}

- (void)concludeDragOperation:(nullable id <NSDraggingInfo>)sender{


}
- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

@end
