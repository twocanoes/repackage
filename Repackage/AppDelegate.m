//
//  AppDelegate.m
//  Repackage
//
//  Created by Timothy Perfitt on 4/20/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "AppDelegate.h"
#import <RepackageFramework/RepackageFramework.h>
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    NSString *path=[[NSBundle mainBundle] pathForResource:@"Defaults" ofType:@"plist" inDirectory:nil];

    NSDictionary *defaultDict=[NSDictionary dictionaryWithContentsOfFile:path];

    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];

    [ud registerDefaults:defaultDict];


    NSDate *lastTimePromoShown=[[NSUserDefaults standardUserDefaults] objectForKey:@"lastTimePromoShown"] ;

    if (lastTimePromoShown==nil) {

        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastTimePromoShown"]; lastTimePromoShown=[NSDate date]; }
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"hidePromo"] boolValue]==NO && (fabs([lastTimePromoShown timeIntervalSinceNow])>60*60*24*7) ) {
//        [[TCSPromoManager sharedPromoManager] showPromoWindow:self];
//
//        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastTimePromoShown"];
//    }
    TCSRepackage *repackage=[[TCSRepackage alloc] init];


}
- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender{
    return YES;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (void)application:(NSApplication *)sender openFiles:(NSArray<NSString *> *)filenames{


    [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSFilesDropped" object:self userInfo:@{@"filenames":filenames}];

}
@end
