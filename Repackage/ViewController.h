//
//  ViewController.h
//  Repackage
//
//  Created by Timothy Perfitt on 4/20/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCDragView.h"
@interface ViewController : NSViewController <TCItemDraggedProtocol>


@end

