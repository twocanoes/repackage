//
//  TCSPreferenceViewController.m
//  Repackage
//
//  Created by Timothy Perfitt on 4/22/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//
#import <SecurityInterface/SFChooseIdentityPanel.h>
#import <SecurityInterface/SFCertificatePanel.h>
#import "TCSPreferenceViewController.h"

@interface TCSPreferenceViewController ()
@property (nonatomic,strong) NSString *serverCertificateText;

@end

@implementation TCSPreferenceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
- (IBAction)showIdentityButtonPressed:(id)sender {
    NSArray *returnIdentityArray;
    OSStatus sanityCheck;
    sanityCheck = SecItemCopyMatching((CFDictionaryRef)[NSDictionary dictionaryWithObjectsAndKeys:
                                                        (id)kSecClassIdentity,           kSecClass,
                                                        kSecMatchLimitAll,      kSecMatchLimit,
                                                        kCFBooleanTrue,         kSecReturnRef,
                                                        kCFBooleanFalse,         kSecReturnAttributes,
                                                        nil
                                                        ] , (void *)&returnIdentityArray);




    if (sanityCheck!=noErr) {
        NSLog(@"SecIdentityCopyCertificate error");
        return ;
    }




    SFChooseIdentityPanel *panel=[SFChooseIdentityPanel sharedChooseIdentityPanel];

    [panel setAlternateButtonTitle:@"Cancel"];
    NSInteger ret=[panel runModalForIdentities:returnIdentityArray message:@"Select identity for signing macOS packages."];

    if (ret==NSModalResponseOK) {
        SecIdentityRef selectedIdentity=panel.identity;
        CFRetain(selectedIdentity);
        SecIdentityRef identity=panel.identity;
        OSStatus            err;
        SecCertificateRef   certificate;
        CFStringRef         summary;

        err = SecIdentityCopyCertificate(identity, &certificate);
        summary = SecCertificateCopySubjectSummary(certificate);
        NSString *identityCertificateText=(__bridge NSString *)(summary);

        [[NSUserDefaults standardUserDefaults] setObject:identityCertificateText forKey:@"signingIdentity"];
    }

}

@end
