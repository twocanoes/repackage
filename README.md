![App Icon](https://twocanoes-app-resources.s3.amazonaws.com/repackage/graphics/repackageicon_small.png)

###Video
[![Video](http://img.youtube.com/vi/Ah_slHf2abA/0.jpg)](http://www.youtube.com/watch?v=Ah_slHf2abA)


Repackage is a macOS app (10.12 or later) that makes it easy to create packages from Applications, Disk Images, or other packages. It came out of the work done on [MDS](https://bitbucket.org/twocanoes/macdeploystick) for converting items to work with startosinstall on macOS. The Repackage project consists of:

1. macOS app 
1. Command Line Tool
1. Framework
1. Static Library

###macOS Application
The macOS application is used for converting packages or for packaging up Disk Images that contain packages, or for packaging up macOS Applications. 

![App Icon](https://twocanoes-app-resources.s3.amazonaws.com/repackage/graphics/repackage-screenshot.png)

##### Using the macOS Application
You can either drag items on the Repackage icon or open the application and drag the items onto the main application window. You can drag as many items at a time as you want, and they will be assesses and converted (if needed) and saved to subfolder named "converted" in the same folder as the original item. The output will always be a meta-flat package. If the original is also a meta flat package, no conversion will take place but the original will be copied to the converted folder. If you set the "force" preference in settings, all packages will be wrapped as a payload inside a meta-flat package (see below).

Depending on the type of item dropped on Repackage, it will be packaged in slightly different ways:

* macOS Application: The application will be packaged up and installed in /Applications when the package is installed.

* Disk Image: The disk image will be added as a payload for the package and a postinstall script added to mount and install any packages in the Disk Image. All Disk Images are converted to sparse disk images (unless they are already a sparse disk disk image) due to the lack of support for large single file payloads in packages. When the resulting package is installed, the payload will be installed in {Target}/tmp, the disk image mounted from tmp, and the postinstall script will install any and all pkgs at the top level of the mounted disk image.

* Bundle Packages (pkg and mpkg): Bundle Packages are added as payload to the new package, and a postinstall script is added to install the bundle package. When the resulting package is installed, the payload will be installed in {Target}/tmp and the postinstall script will install the bundle package from tmp to the target.

* Flat Packages: Flat packages are packaged up inside a meta flat package unless the force preference is set. If that preference is set, the flat package is installed like a bundle package.

* Meta Flat Packages: Meta Flat Packages are not converted and are just copied unless the force preference is set. If that preference is set, the flat package is installed like a bundle package. 

###Command Line Tool
The command line tool is provided inside the macOS application, but does not depend on any resources from the application. It can be moved and run from any location. The command line tool, repackage-cli, is located in the MacOS inside the application bundle, and converts the same way as the macOS application. 

##### Using the repackage-cli
Pass an item (package, disk image, or application) to the command line utility: 

`/Applications/Repackage.app/Contents/MacOS/repackage-cli /path/to/item`

The item will be packaged up in a subfolder ("converted") the same as the macOS app. If you specify the force flag ("-f"), all flat packages will be wrapped as payload instead of added to a meta flat package:

`/Applications/Repackage.app/Contents/MacOS/repackage-cli -f /path/to/item`

###macOS Framework
All of the functionality in repackage is part of the framework and can be added to any macOS application. It is intended to be used by [MDS](https://bitbucket.org/twocanoes/macdeploystick) with Carthage.

###macOS Static Library
repackage can also be compiled as a static library, mainly for removing the framework dependency for the command line tool.

###Building
To build, run carthage to build the dependencies:

./carthage update

Then open Xcode and build the main app.

### License
Copyright 2019 Twocanoes Software, Inc

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
